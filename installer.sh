#!/bin/sh

export DEVICE_ID={device_id}
export ANSIBLE_HOST_KEY_CHECKING=False
apt-get install sshpass -y
if [ ! -d ./installer ]; then
  git clone https://gitlab.com/hashscreen/installer.git
fi
cd installer
ansible-playbook -i hosts pi.yml -k
cd ..
rm -rf installer